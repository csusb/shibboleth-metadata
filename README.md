# README

## Adding Metadata

See [CONTRIBUTING](CONTRIBUTING.md) for general guidelines.

### Via Bitbucket Web

1.  By default, you should be on the `next` branch in the "Source" view. If not,
    change it to `next`.

2.  Expand the ellipse menu in the upper right corner of the source view and
    select "Add file".

3.  On the new file page:
    * Give the file a name ending with an `.xml` extension (e.g. `foobar-metadata.xml`).
    * Paste the metadata XML contents into document body.
    * Click "Commit" when done.

4.  In the "Commit changes" dialog:
    * Give a better commit message than the default (i.e. explain the why not the what).
    * Leave "Create a pull request for this change" unchecked.
    * Click "Commit" when done.

5.  The change will automatically be deployed to the testing environment, and a
    notice will be sent to the #iam csusb-it Slack channel.

6.  Please open an ticket in the "Issues" view to help keep track of the
    integration processes. Describe what the service is and include any
    documentation or other technical information that would be helpful. The
    more through you are here, the smoother the integration will be
    (hopefully).

## Promote Staging Aggregate to Production

Invoke the `promote` task with `make promote`. This will require an AWS profile
configured with permissions to assume `ops/Read-Only` and `ops/Operations`
roles in the security account. Ensure your iam profile is default or set
`AWS_DEFAULT_PROFILE` as needed.

This will promote the version of the staging aggregate last promoted from
testing via the build pipeline. Since anyone with write access the repo can run
the pipelines steps, ensure that the staging aggregate published is indeed the
one desired to be published to production.
