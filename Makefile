# Makefile

.PHONY: all clean test promote

all: metadata-aggregate.xml

S3_BUCKET_URL=s3://csusb-shibboleth-metadata

metadata-aggregate.xml: aggregate.xsl aggregate.xml
	xsltproc --output $@ aggregate.xsl aggregate.xml

metadata-aggregate.d: aggregate.xml depends.xsl
	xsltproc --output $@ depends.xsl aggregate.xml

catalog.xml: xsd/xmldsig-core-schema.xsd xsd/xenc-schema.xsd xsd/xml.xsd
	xmlcatalog --noout --create $@
	xmlcatalog --noout --add uri http://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd xsd/xmldsig-core-schema.xsd $@
	xmlcatalog --noout --add uri http://www.w3.org/TR/2002/REC-xmlenc-core-20021210/xenc-schema.xsd xsd/xenc-schema.xsd $@
	xmlcatalog --noout --add uri http://www.w3.org/2001/xml.xsd xsd/xml.xsd $@

test: metadata-aggregate.xml catalog.xml
	find . -type f -name '*-metadata.xml' | XML_CATALOG_FILES=./catalog.xml xargs xmllint --schema xsd/saml-schema-metadata-2.0.xsd --noout
	XML_CATALOG_FILES=./catalog.xml xmllint --schema xsd/saml-schema-metadata-2.0.xsd --noout metadata-aggregate.xml

clean:
	rm -f metadata-aggregate.xml
	rm -f catalog.xml
	rm -f *.d

promote:
	aws sts get-caller-identity --output text --query Arn || echo Perhaps AWS_DEFAULT_PROFILE is not set?
	bin/as_role arn:aws:iam::682414671842:role/ops/ReadOnly \
		bin/s3_diff ${S3_BUCKET_URL} metadata-aggregate.xml metadata-aggregate-staging.xml
	@read -rp "Promote staging to production? " REPLY && case "$$REPLY" in [yY][eE][sS]) true;; *) false;; esac
	bin/as_role arn:aws:iam::682414671842:role/ops/Operations \
		aws s3 cp ${S3_BUCKET_URL}/metadata-aggregate-staging.xml ${S3_BUCKET_URL}/metadata-aggregate.xml

include metadata-aggregate.d
