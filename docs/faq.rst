Frequently Asked Questions
==========================

What is Shibboleth and how does it work?
----------------------------------------

Shibboleth is a reference implementation of the `SAML2`_ standard.

`SAML2`_ is expressed as a series of XML documents passed between a service
provider and an identity provider. `SAML2`_ relies entirely on digital signatures
to authenticate the messages passed between components.

.. _SAML2: https://en.wikipedia.org/wiki/SAML_2.0


What is an IdP?
---------------

An IdP (Identity Provider) is the server component that the campus maintains
that allows other services to request authentication and authorization
information for people in our organization.


What is our IdP’s EntityID?
---------------------------

https://idp.csusb.edu/idp/shibboleth


A vendor is asking for “metadata”?
----------------------------------

If the vendor is a member of `InCommon`_, all the vendor likely needs is our
IdPs EntityID. Any `InCommon`_ member organization's service should already
have our metadata.

Alternatively, the metadata for our IdP can be fetched using the `EntityID`_ as
a URL. We strongly discourage use of this mechanism in preference of using an
aggregate feed.

⚠️ Please do not dynamically fetch metadata feeds unless it is via https://
and you are certain that the software doing the fetching properly validates
https:// connections or, alternatively, that the metadata is signed and the
software is configured to validate that signature. `InCommon`_ metadata is
signed.

.. _InCommon: https://spaces.at.internet2.edu/display/federation/Metadata+Service
.. _EntityID: https://idp.csusb.edu/idp/shibboleth


Where is the InCommon metadata feed?
------------------------------------

`InCommon`_ maintains a few different feeds. If you’re running a service provider,
you will most likely want the InCommon IdP-only metadata feed.

Though, downloading the full feed is deprecated in favor of the metadata query
(MDQ) service.


I would like to add a service to Shibboleth. Where to start?
------------------------------------------------------------

All integrations start by going against our test IdP. It is mostly identical to
the production IdP except for endpoint URLs and signing keys. A key point is
that the EntityID is the same; this facilitates switching between test and
production by only requiring a swap of metadata sources. This helps ensure a
smooth transition to production by keeping configuration changes down to a
minimum.

See How do I configure my service provider to use the test IdP? for directions
on pointing the SP at the test environment.


I’m writing a custom application. How do I add SAML support?
------------------------------------------------------------

Depending on the architecture of the application, this may require implementing
a service provider within the application. There are a number of SAML libraries
to aid in development. We cannot provide support for proprietary
implementations.

There is a generic service provider implementation in the form of an apache2
saml module. This requires either implementing your application as an apache
module or by devising a secure method to delegate authorization to apache.


How do I configure my service provider to use the test IdP?
-----------------------------------------------------------

Configure the service provider to use the `Test IdP`_ metadata file. The
EntityID remains unchanged from the production IdP. The test environment does
not use the InCommon metadata aggregate.

Additionally, the service provider’s own metadata will need to be provided to
the IdP. See How do I submit metadata for my Service Provider?

The testing IdP uses randomized `Test Credentials`_ based on a snapshot production.
Users will not be able to login without using the credential lookup interface
(see Are there test credentials a vendor can use for testing?).

.. _Test IdP: https://weblogon.dev.csusb.edu/idp/shibboleth

How do I submit metadata for my Service Provider?
-------------------------------------------------

If the SP is published in the InCommon aggregate, there is nothing to do.
Otherwise, this is done by submitting the metadata via Bitbucket.
Alternatively, you can post it as a snippet in the Slack #iam channel or email
the metadata file to iset-ops@csusb.edu. Using Bitbucket is strongly preferred
and will likely have a shorter turn-around time.

If you are using the “native” Shibboleth service provider, you can use the
guide on generating the metadata file.

⚠️ The generated metadata is a starting point and should not be considered
production ready.


Are there credentials a vendor can use for testing?
---------------------------------------------------

Navigate to the `Test Credentials`_ retrieval page. You will be directed to
production single sign-on page to authenticate.

These credentials are actual CoyoteIDs with randomized passwords and are
refreshed daily. The intent is that a tester can choose a CoyoteID with a
certain set of attributes to satisfy a given testing scenario. The interface
permits a tester to lookup any arbitrary CoyoteID. These credentials only work
for the testing IdP.  Test credentials are not provided for the production IdP.

Access to the credential lookup interface requires the tester to be an active
employee or contractor. Access is restricted to source addresses within the
campus address spaces. Contractors can gain access via the campus VPN.

.. _Test Credentials: https://weblogon.dev.csusb.edu/credentials


Is there a way our application can logout using the IdP?
--------------------------------------------------------

The short answer is no.

The longer answer is that the IdP isn’t a session manager for the application.
The “single” part of SSO relies on the IdP session persisting across
applications so that access to multiple applications does not require
re-authenticating. The usual technique for avoiding this issue is to have the
application logout function redirect the user somewhere where it will not cause
the user to “loop” back into the application.

The URL https://www.csusb.edu/its/support/logout can be used if other options
do not exist.


What about SLO (Single Sign Off)?
---------------------------------

SLO is not supported. SLO only works reliably in tightly controlled
environments where the “logout” semantics are well understood and agreed upon
between applications. Our environment does not match that criteria. We support
a number of applications that experience data loss when the application session
is terminated externally (e.g. Blackboard LMS) by another application
triggering the SLO function.


Why am I getting an “untrusted party” or “unsupported request” error?
---------------------------------------------------------------------

The IdP does not have a metadata entry for the SP that is requesting
authentication services.  See How do I submit metadata for my Service Provider?

.. _Attributes:

What attributes are available?
------------------------------

We provide attributes for a suite of common data which includes names, email
addresses and affiliation. These attributes are released by default to InCommon
registered SPs and abide by standards defined by InCommon and the CSU System.
These attribute definitions largely mimic the X.500 (LDAP) standards and use
the same OID numbers for the SAML attribute encoding.

These are the most common attributes requested. The value in square brackets is
the actual (real) attribute name as presented “on the wire”. The other value is
the so-called “friendly name”. Most software requires referencing by the real
name.


eduPersonPrincipalName [urn:oid:1.3.6.1.4.1.5923.1.1.1.6]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Contains an immutable identifier for a given user in the form of a domain
qualified value. Typically, this will be the CoyoteID qualified with the campus
domain (e.g. <coyote-id>@csusb.edu).

⚠️This value should not be interpreted as an email address.


uid [urn:oid:0.9.2342.19200300.100.1.1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Contains an identifier for a given user. Typically, this will be the CoyoteID;
however, it doesn’t have to be.


calstateEduPersonEmplID [urn:oid:1.3.6.1.4.1.10396.2.1.1.3]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Contains the EMPLID from PeopleSoft HR/CS. This is also known as the CoyoteID.


employeeNumber [urn:oid:2.16.840.1.113730.3.1.3]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Has the same semantics as calstateEduPersonEmplID above.


displayName [urn:oid:2.16.840.1.113730.3.1.241]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The user’s preferred name. Typically this is the legal first and last name
joined with a space unless the value is requested to be changed by the user.


mail [urn:oid:0.9.2342.19200300.100.1.3]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The email address of the user. In most cases, this will be the Exchange mailbox
for staff and faculty. It will otherwise be the student’s coyote.csusb.edu
address.


eduPersonAffiliation [urn:oid:1.3.6.1.4.1.5923.1.1.1.1]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a multi-valued attribute that identifies the user’s affiliation with
the campus in broad categories (Employee, Staff, Faculty, Student, etc).


givenName [urn:oid:2.5.4.42]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Also known as a first name. Using displayName is preferred where possible.


surname [urn:oid:2.5.4.4]
~~~~~~~~~~~~~~~~~~~~~~~~~

Also known as lastname or family name. Using displayName is preferred where
possible.

Does CSUSB support `encryptAssertions="true"`?
----------------------------------------------

We can support metadata containing `<SPSSODescriptor encryptAssertions="true" ... >`.  However, we do not require it.

The `SAML2`_ default is `encryptAssertions=false`. We recommend keeping the
default to facilitate the use of troubleshooting tools like `SAML Tracer
<https://addons.mozilla.org/en-US/firefox/addon/saml-tracer/>`_.

The :ref:`attributes CSUSB is willing to release <Attributes>`
to a relying party are considered directory information which are already known
to the principal (end user).  We don't include secrets within the assertion.

What are the common NameIDFormat options (the assertion subject value)?
-----------------------------------------------------------------------

urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified [default]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If a Service Provider requests the unspecified name format, it's an indication
to our Identity Provider that the SP is not interested in value provided in the
Subject.  We will send a TransientID.

Example:

.. code-block:: xml

    <Subject>
        <NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified">pZGm1Av0IEBKARczz7exkNYsZb8LzaMrV7J32a2fFG4=</NameID>
        [ ... ]
    </Subject>

urn:oasis:names:tc:SAML:2.0:nameid-format:persistent [preferred]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We will send eduPersonPrincipalName as the assertion Subject.

.. code-block:: xml

    <Subject>
        <NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:persistent">000226420@csusb.edu</NameID>
        [ ... ]
    </Subject>


urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress  [not recommended]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is *not* a substitute for a persistent identifier.  Attempting to use
emailAddress as a unique or persistent identifier is problematic as email
addresses are based on given name and surnames and are expected to change over
the lifetime of the user for many reasons:

* students become employees
* employees become students
* people get married
* people divorce
* people decide to go by their nickname or middle name
* people change their hyphenation
* people obtain vanity email address to help distinguish themselves

Instead, utilize the persistent name format and rely on the mail assertion
attribute to obtain a user's current email address.

.. code-block:: xml

    <!-- we can do this, but this will cause problems  -->
    <Subject>
        <NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress">Joe.Coyote@csusb.edu</NameID>
        [...]
    </Subject>
