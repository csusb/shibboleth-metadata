# encoding: utf-8

from datetime import datetime

project = u'CSUSB Shibboleth Metadata'
version = 'latest'
release = 'latest'
copyright = str(datetime.now().year)
extensions = []
templates_path = []
source_suffix = ['.rst']
master_doc = 'index'
exclude_patterns = ['_build']
pygments_style = 'sphinx'
htmlhelp_basename = 'csusb-shibboleth-metadata'
html_theme = 'sphinx_rtd_theme'
file_insertion_enabled = False
