# CONTRIBUTING

## Git Workflow

Changes should be brought in initially by pushing directly onto the `next`
branch. Changes to this branch will automatically trigger a testing deployment.

Once a unit of work is brought to satisfactory point, create a feature branch
off `next`, and perform an an interactive rebase against `master`. Strip
commits not related to the unit of work. Further rebase this branch to collapse
it down to a single commit if possible.

This topic branch is then submitted via pull-request for inclusion into
`master`.

If the above sounds like gibberish, drop a note by the #iam channel for
assistance.

## Topic Branch Naming

Create a new branch using the scheme `integration/<name>` where `<name>` should
be a descriptive name (e.g. the name of the service or entity). If you have
already opened a pull-request with a non-conformant branch name, the
pull-request will need to be declined and a new one opened against the renamed
branch.

## Adding New Metadata Files

1. Create a new `xml` file named more-or-less after the new service provider,
   e.g. `kenyan-tutors.org-metadata.xml`

  * Ensure AssertionConsumerService entries are pointing to real URLs (and not
    localhost)

  * Ensure X509Certificate is self-signed and has an appropriate expiration

  * EntityIds should when possible be crafted to match the URL pattern
    `https://metadata.csusb.edu/<service_provider_name>/shibboleth`.

2. Add an include entry to `aggregate.xml` based on the new filename, e.g.
   `<include path=metadata/kenyan-tutors.org-metadata.xml"/>`
