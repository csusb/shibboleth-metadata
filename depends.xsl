<?xml version="1.0" encoding="utf-8"?>
<!-- vim: expandtab ts=2 sw=2
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:metadata="http://csusb.edu/xml/metadata">

  <xsl:output method="text" encoding="utf-8" />

  <xsl:template match="metadata:aggregate">
    <xsl:text>metadata-aggregate.xml: </xsl:text>
    <xsl:for-each select="metadata:include">
      <xsl:value-of select="@path"/>
      <xsl:if test="position() != last()">
        <xsl:text>&#x20;</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>
</xsl:stylesheet>
