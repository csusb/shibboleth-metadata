<?xml version="1.0" encoding="utf-8"?>
<!-- vim: expandtab ts=2 sw=2
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:metadata="http://csusb.edu/xml/metadata"
  xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
  xmlns:mdrpi="urn:oasis:names:tc:SAML:metadata:rpi"
  xmlns:mdattr="urn:oasis:names:tc:SAML:metadata:attribute">

  <xsl:output method="xml" encoding="utf-8" indent="yes" />

  <xsl:param name="federation-name">https://metadata.csusb.edu</xsl:param>

  <xsl:template match="*">
    <xsl:message terminate="yes">
      Unmatched element: <xsl:value-of select="name()" />
    </xsl:message>
  </xsl:template>

  <xsl:template match="/">
    <md:EntitiesDescriptor Name="{$federation-name}">
      <xsl:apply-templates />
    </md:EntitiesDescriptor>
  </xsl:template>

  <xsl:template match="metadata:aggregate">
    <xsl:apply-templates select="metadata:include" />
  </xsl:template>

  <xsl:template match="metadata:include">
    <xsl:apply-templates select="document(@path)//md:EntityDescriptor" />
  </xsl:template>

  <xsl:template match="md:EntityDescriptor">
    <xsl:copy>
      <xsl:for-each select="@*">
        <xsl:copy />
      </xsl:for-each>
      <md:Extensions>
        <mdrpi:RegistrationInfo registrationAuthority="{$federation-name}" />
        <xsl:copy-of select="md:Extensions/mdattr:EntityAttributes" />
      </md:Extensions>
      <xsl:copy-of select="md:SPSSODescriptor" />
      <xsl:copy-of select="md:Organization" />
      <xsl:copy-of select="md:ContactPerson" />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
